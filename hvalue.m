function [ h ] = hvalue( particledensity,dist)
%input 


%%%%%%%%%%%%%  below calculations just calculate h values for different
%%%%%%%%%%%%%  particle densities, I have just saved those values so the
%%%%%%%%%%%%%  loops dont need to be run every time.....






% if particledensity==4
%     h=(dist+sqrt(2)*dist)/2;
% elseif particledensity==8
%     h=(2*dist+sqrt(2)*dist)/2;
% elseif particledensity==12
%     h=(2*dist+sqrt(5)*dist)/2;
% elseif particledensity==20
%     h=(2*sqrt(2)*dist+sqrt(5)*dist)/2;
% elseif particledensity==28
%     h=(2*sqrt(2)*dist+sqrt(9)*dist)/2;
% elseif particledensity==32
%     h=(sqrt(10)*dist+sqrt(9)*dist)/2;
% elseif particledensity==40
%     h=(sqrt(13)*dist+sqrt(10)*dist)/2;
% elseif particledensity==48
%     h=(sqrt(13)*dist+sqrt(10)*dist)/2;
% elseif particledensity==48
%     h=(sqrt(13)*dist+sqrt(18)*dist)/2;
% elseif particledensity==56
%     h=(sqrt(13)*dist+sqrt(17)*dist)/2;
% elseif particledensity==60
%     h=(sqrt(17)*dist+sqrt(18)*dist)/2;
% elseif particledensity==68
%     h=(sqrt(18)*dist+sqrt(20)*dist)/2;
% elseif particledensity==76
%     h=(sqrt(20)*dist+sqrt(25)*dist)/2;
% elseif particledensity==84
%     h=(sqrt(26)*dist+sqrt(25)*dist)/2;
% elseif particledensity==92
%     h=(sqrt(26)*dist+sqrt(29)*dist)/2;
% elseif particledensity==100
%     h=(sqrt(20)*dist+sqrt(25)*dist)/2;
% l=1;
% 
% for i=1:10
%     for j=1:10
%         h(l)=sqrt(i^2+j^2);
%     l=l+1;
%     end
% h(l)=sqrt(i^2);
% l=l+1;
% end
% 
% h=sort(h);
% k=2;
% particless(1)=4;
% i=2;
% while i<=size(h,2)-1
%     if abs(h(i+1)-h(i))==0
%     particless(k)=particless(k-1)+8;
%     k=k+1;
%     i=i+2;
%     else
%     particless(k)=particless(k-1)+4;
%     k=k+1;
%     i=i+1;
%     end  
%     fprintf('%.0f ',particless(k-1));
% end
% k=1;
% i=1;
% while i<=size(h,2)-1
%     if abs(h(i+1)-h(i))< 1*10^-10
%     hvals(k)=h(i);
%     k=k+1;
%     i=i+2;
%     else
%     hvals(k)=h(i);
%     k=k+1;
%     i=i+1;
%     end  
%     %fprintf('%.12f ',hvals(k-1));
% end
particless=[4 8 12 20 24 28 36 44 48 56 60 68 76 80 88 96 100 108 112 120 128 136 144 148 156 160 168 176 184 192 196 204 212 220 224 232 240 248 252 260 268 276 284 292 300 304 312 316 324 332 340 348 356 364 372 380 384 392 400 408 416 420 428 436];
h=[1.000000000000 1.414213562373 2.000000000000 2.236067977500 2.828427124746 3.000000000000 3.162277660168 3.605551275464 4.000000000000 4.123105625618 4.242640687119 4.472135955000 5.000000000000 5.000000000000 5.099019513593 5.385164807135 5.656854249492 5.830951894845 6.000000000000 6.082762530298 6.324555320337 6.403124237433 6.708203932499 7.000000000000 7.071067811865 7.071067811865 7.211102550928 7.280109889281 7.615773105864 7.810249675907 8.000000000000 8.062257748299 8.062257748299 8.246211251235 8.485281374239 8.544003745318 8.602325267043 8.944271909999 9.000000000000 9.055385138137 9.219544457293 9.219544457293 9.433981132057 9.486832980505 9.848857801796 9.899494936612 10.000000000000 10.000000000000 10.049875621121 10.198039027186 10.295630140987 10.440306508911 10.630145812735 10.770329614269 10.816653826392 11.180339887499 11.313708498985 11.401754250991 11.661903789691 12.041594578792 12.206555615734 12.727922061358 12.806248474866 13.453624047074];
for i=1:length(particless)
if particledensity==particless(i)
    h=h(i)*dist;  % some kernels have 2h or more this will have to be taken into account.....
end
end
