t=datetime('now');
DateString = datestr(t);
directoryname = strcat(DateString,sprintf('_h=%g_particles=%g_dist=%g_iterations=%g',h,particles,dist,totaliterations));
[s,mess,messid] = mkdir('Videos/',directoryname);
path=strcat('Videos/',directoryname);

writerObj = VideoWriter(directoryname,'Uncompressed AVI');
%writerObj.FrameRate=round(1/dt);
open(writerObj);
for K = 0:iterations
  filename = sprintf('%d.png', K);
  thisimage = imread(filename);
  writeVideo(writerObj, thisimage);
  movefile(filename, path);
end
close(writerObj);
movefile(strcat(directoryname,'.avi'), path);
clc
 