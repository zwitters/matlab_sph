    function [ dwdx, dwdy ] = gradW2(xij, yij, rij, h )

    
    



%%%%%%%%%%%%%%%%%%%%%%%%%%  cubic kernel %%%%%%%%%%%%%%%%%%%%%
% % % alpha= 10/(7*pi()*h^3);
% % % if rij>1*10^-12
% % %     if rij/h<=1
% % %        W= alpha*((9/4)*(rij/h)^2-3*(rij/h))/(rij);
% % %     elseif rij/h<=2
% % %        W= alpha*(-(3/4)*(2-rij/h)^2)/(rij);
% % %     else
% % %             W=0;
% % %     end
% % % else
% % %      W=0;
% % % end

%%%%%%%%%%%%%%%%%%%%%%%%%% "spiky"  kernel %%%%%%%%%%%%%%%%%%%%%
% % % 
% % % if rij/h<=1
% % %     W= -15*3/(1.5*pi()*h^6)*(h-rij)^2;
% % % else
% % %     W=0;
% % % end
% % % else
% % %       W=0;
% % % end


%%%%%%%%%%%%%%%%%%%%%%%%%% Wend  kernel %%%%%%%%%%%%%%%%%%%%%
% % % alpha=7/(4*pi()*h^3);
% % % if rij>1*10^-12
% % %     if rij/h<=2
% % %        W=(alpha*(2*(rij/(2*h)-1)^4+2*(2*rij/h+1)*(rij/(2*h)-1)^3))/rij;
% % %     else
% % %             W=0;
% % %     end
% % % else
% % %     W=0;
% % % end

%%%%%%%%%%%%%%%%%%%%%%%%%% Wendlandquintic kernel %%%%%%%%%%%%%%%%%%%%%
alpha=7/(4*pi()*h^3);
if rij>1*10^-12
    if rij/h<=2
       W=(alpha*(-5*rij/h*(1-.5*rij/h)^3))/rij;
    else
            W=0;
    end
else
    W=0;
end



    dwdx=W*xij;
    dwdy=W*yij;
     end
