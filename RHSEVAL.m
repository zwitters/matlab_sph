function[rho, velocity]= RHSEVAL( pp, mass ,pressure, h, alpha, beta, cs, epsilon, particles,rho0,gravity,rhorhs,dt,velocity1,rho1,rho,velocity,totalforce)

dpdx=0;
dpdy=0;
rhoval=0;
visx=0;
visy=0;
tmpvis=0;
tmprhoj=0;
mu=.547*10^-3; %Dynmamic Viscosity of water at 50C
% sumrij=0;
% Rx=0;
% Wij=0;
% Ry=0;
tmpmassj=mass(1);
idx= rangesearch(pp,pp,2*h);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% force eval
for i=1:size(pp,1)
    
    
    tmprhoi=rho(i); %%% name tmp variables for i to avoid accessing them a lot
    ppi1=pp(i,1);
    ppi2=pp(i,2);
    veli1=velocity(i,1);
    veli2=velocity(i,2);
    tmppresi=pressure(i);
    if tmppresi<0
        tmppresi=0;
    end
    
    for k=1:size(idx{i},2) %% start the loop for neighbors
        
        j=idx{i}(k);
        %tmpmassj=mass(j);
        rx=ppi1-pp(j,1);
        ry=ppi2-pp(j,2);
        rij=sqrt((rx)^2+(ry)^2);
        dv1=(veli1-velocity(j,1));
        dv2=(veli2-velocity(j,2));
        tmppresj=pressure(j);
        if tmppresj<0
            tmppresj=0;
        end
        tmprhoj=rho(j);

        
        
        %dmg=rij/h
        %%%%%%%%%%%%%%%%%%% gradient eval %%%%%%%%%%
        [dwdx,dwdy]=gradW2(rx,ry,rij,h);
        
        %%%%%%%%%%%%%%%%%%%%     continuity eval
        rhoval=rhoval+tmpmassj*((dv1)*dwdx+(dv2)*dwdy);  % continuity equation
        %%%%%%%%%%%% pressure eval    %%%%%%%%%%%%%
%         pval=tmpmassj*(tmppresi/tmprhoi^2+tmppresj/tmprhoj^2);
%         dpdx= dpdx+pval*dwdx;
%         dpdy= dpdy+pval*dwdy;

        pval=tmpmassj*mass(i)/tmprhoi/tmprhoj*(tmppresi+tmppresj);
         dpdx= dpdx+pval*dwdx;
         dpdy= dpdy+pval*dwdy;
        
        
        
        %%%%%%%%%%%%%%%%%%   Viscous eval (artificial viscosity)  %%%%%%%%%%%
%         vdotrij=(dv1)*rx+(dv2)*ry;
%         if (vdotrij)<0
%             tmpvis=0;
%             uij= h*vdotrij/(rij^2+epsilon*h^2);
%             rhoavg=(tmprhoi+tmprhoj)/2;
%             
%             tmpvis=-alpha*cs*uij/rhoavg*tmpmassj;   %%%%% linear term
%             tmpvis= tmpvis+beta*uij^2/rhoavg*tmpmassj;   %%%%% squared term
%             visx=visx+tmpvis*dwdx;
%             visy=visy+tmpvis*dwdy;
%         end
        
if rij>1*10^-10
vdotrij=(dv1)*rx+(dv2)*ry;
tmpvis=vdotrij*tmpmassj/tmprhoi/tmprhoj/(rij^2);
visx=visx+tmpvis*dwdx;
visy=visy+tmpvis*dwdy;
end

        %%%%%%%%%%%%%%%%%%  artifical pressure (tensile instability term)  %%%%%%%%
        % % sumrij=sumrij+rij;
        % % Wij=Wkernel(rij,h)^4+Wij;
        % % R=tmpmass*(abs(pressure(i))/rho(i)^2+abs(pressure(j))/rho(j)^2);
        % % Rx=R*dwdx+Rx;
        % % Ry=R*dwdy+Ry;
        
    end   %%% end of j particles iteration
    % %    fab=Wij/(Wkernel(sumrij/k,h)^4);
    % % Rijfijx=Rx*.01*fab;
    % % Rijfijy=Ry*.01*fab;
    % %
    totalforce(i,:)=[-dpdx-8*mu*visx/tmprhoi,-dpdy-8*mu*visy/tmprhoi+gravity];
    rhorhs(i)=rho1(i)+rhoval*dt;
    if i>particles
        if rhorhs(i)<rho0
            rhorhs(i)=rho0;
        end
    end
    
    %     if i>particles
    %         nrho=rho(i)+rhorhs(i)*dt;
    %         if nrho < rho0
    %             rho(i)=rho0;
    %         else
    %             rho(i)=nrho;
    %         end
    %     else
    %         rho(i)= rho(i) +rhorhs(i)*dt;
    %     end
    
    
    
    dpdx=0;  % resetting summations.....
    dpdy=0;
    rhoval=0;
    visx=0;
    visy=0;
    %     Rx=0;
    %     Ry=0;
    %     sumrij=0;
    %     Wij=0;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
velocity(1:particles,:)= velocity1(1:particles,:)+totalforce(1:particles,:)*dt;
rho=rhorhs;


%%%%%%%%%%%%%%%%%    second iteraiton...... %%%%%%%%%%%%%%%%%
for i=1:size(pp,1)
    
    tmprhoi=rho(i); %%% name tmp variables for i to avoid accessing them a lot
    ppi1=pp(i,1);
    ppi2=pp(i,2);
    veli1=velocity(i,1);
    veli2=velocity(i,2);
    tmppresi=pressure(i);
    if tmppresi<0
        tmppresi=0;
    end
    
    for k=1:size(idx{i},2) %% start the loop for neighbors
        
        j=idx{i}(k);
        %tmpmassj=mass(j);
        rx=ppi1-pp(j,1);
        ry=ppi2-pp(j,2);
        rij=sqrt((rx)^2+(ry)^2);
        dv1=(veli1-velocity(j,1));
        dv2=(veli2-velocity(j,2));
        tmppresj=pressure(j);
        if tmppresj<0
            tmppresj=0;
        end
        tmprhoj=rho(j);

        
        
        %dmg=rij/h
        %%%%%%%%%%%%%%%%%%% gradient eval %%%%%%%%%%
        [dwdx,dwdy]=gradW2(rx,ry,rij,h);
        
        %%%%%%%%%%%%%%%%%%%%     continuity eval
        rhoval=rhoval+tmpmassj*((dv1)*dwdx+(dv2)*dwdy);  % continuity equation
        %%%%%%%%%%%% pressure eval    %%%%%%%%%%%%%
%         pval=tmpmassj*(tmppresi/tmprhoi^2+tmppresj/tmprhoj^2);
%         dpdx= dpdx+pval*dwdx;
%         dpdy= dpdy+pval*dwdy;
        pval=tmpmassj*mass(i)/tmprhoi/tmprhoj*(tmppresi+tmppresj);
         dpdx= dpdx+pval*dwdx;
         dpdy= dpdy+pval*dwdy;
        


%%%%%%%%%%%%%%%%%%   Viscous eval (artificial viscosity)  %%%%%%%%%%%
%         vdotrij=(dv1)*rx+(dv2)*ry;
%         if (vdotrij)<0
%             tmpvis=0;
%             uij= h*vdotrij/(rij^2+epsilon*h^2);
%             rhoavg=(tmprhoi+tmprhoj)/2;
%             
%             tmpvis=-alpha*cs*uij/rhoavg*tmpmassj;   %%%%% linear term
%             tmpvis= tmpvis+beta*uij^2/rhoavg*tmpmassj;   %%%%% squared term
%             visx=visx+tmpvis*dwdx;
%             visy=visy+tmpvis*dwdy;
%         end
%         

if rij>1*10^-10
vdotrij=(dv1)*rx+(dv2)*ry;  
tmpvis=vdotrij*tmpmassj/tmprhoi/tmprhoj/(rij^2);
visx=visx+tmpvis*dwdx;
visy=visy+tmpvis*dwdy;
end


        %%%%%%%%%%%%%%%%%%  artifical pressure (tensile instability term)  %%%%%%%%
        % % sumrij=sumrij+rij;
        % % Wij=Wkernel(rij,h)^4+Wij;
        % % R=tmpmass*(abs(pressure(i))/rho(i)^2+abs(pressure(j))/rho(j)^2);
        % % Rx=R*dwdx+Rx;
        % % Ry=R*dwdy+Ry;
        
    end   %%% end of j particles iteration
    % %    fab=Wij/(Wkernel(sumrij/k,h)^4);
    % % Rijfijx=Rx*.01*fab;
    % % Rijfijy=Ry*.01*fab;
    % %
    
      totalforce(i,:)=[-dpdx-8*mu*visx/tmprhoi,-dpdy-8*mu*visy/tmprhoi+gravity];
    rhorhs(i)=rho1(i)+rhoval*dt;
    if i>particles
        if rhorhs(i)<rho0
            rhorhs(i)=rho0;
        end
    end
    
    %     if i>particles
    %         nrho=rho(i)+rhorhs(i)*dt;
    %         if nrho < rho0
    %             rho(i)=rho0;
    %         else
    %             rho(i)=nrho;
    %         end
    %     else
    %         rho(i)= rho(i) +rhorhs(i)*dt;
    %     end
    
    
    
    dpdx=0;  % resetting summations.....
    dpdy=0;
    rhoval=0;
    visx=0;
    visy=0;
    %     Rx=0;
    %     Ry=0;
    %     sumrij=0;
    %     Wij=0;
end
velocity(1:particles,:)= velocity1(1:particles,:)+totalforce(1:particles,:)*dt;
rho=rhorhs;
end

