clear all,clc,close all force

%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 2D Dam Particle initialization %%%%%%%%%%%%%%
format longg
%%%%%%%%%%%%%%%%%%%%%%%%%% particle density requirement........
particledensity=44;
particles= 3000; % # of particles
%   this value needs to be one of the following values!!!!
%   [4 8 12 20 24 28 36 44 48 56 60 68 76 80 88 96 100 108 112 120
%   128 136 144 148 156 160 168 176 184 192 196 204 212 220 224 232 240 248
%   252 260 268 276 284 292 300 304 312 316 324 332 340 348 356 364 372
%   380 384 392 400 408 416 420 428 436];
h=.03;
height=2;%distance (vertically and horizontally) between each particle
width=1;
htow=height/width;%this is the height to width ratio.
dist=sqrt(height*width/particles);
% h=(hvalue(particledensity,dist))/2; %% ******* remember this will depend on what kernel you are using if you really want to divide by 2 !!!!!!!!!!!!!!!
fprintf(' h is equal to: %.5f \n',h)
%%%%%%%%%%%%%      inputs %%%%%%%%%%%
dens= 1000; % density of the fluid
dim =2; % enter 2 or 3
height=sqrt(dist^2*particles*htow);
width=height/htow;
containerheight=2*height;
containerwidth=10*width;
pp=zeros(particles,dim);
%%%%%%%%%%%%%%%%%   fluid particle pp init. %%%%%%%%%%%%%%
[x,y]=meshgrid(dist/2:dist:width+dist/2,0:dist:height);
% [x2,y2]=meshgrid(containerwidth-width-dist:dist:containerwidth-dist,0:dist:height);
% [x3,y3]=meshgrid(containerwidth/2-width/4:dist:containerwidth/2+width/4,4*height/3:dist:11*height/6);
%Second Column
i=1;
for n=1:size(x,2)
    for j=1:size(y,1)
        pp(i,:)=[x(1,n) y(j,1)];
        i=i+1;
    end
end
% for n=1:size(x2,2)
%     for j=1:size(y2,1)
%         pp(i,:)=[x2(1,n) y2(j,1)];
%         i=i+1;
%     end
% end
% for n=1:size(x3,2)
%     for j=1:size(y3,1)
%         pp(i,:)=[x3(1,n) y3(j,1)];
%         i=i+1;
%     end
% end

particles=size(pp,1); %%%%% must redefine number of particles for constant grid
bpp=zeros(particles,dim);
%%%%%%%%%%%%%%%%%  boundary particle pp init. %%%%%%%%%%%%%%
[xL,yM]=meshgrid(-2.25*dist:dist:0,-.75*dist:dist:containerheight+2*dist);
[xM,yB]=meshgrid(-2.25*dist:dist:containerwidth+2.25*dist,-2.75*dist:dist:-.75*dist);
[xR,yM2]=meshgrid(containerwidth:dist:containerwidth+2.25*dist,-.75*dist:dist:containerheight+2*dist);
bpparticles=size(xL,2)*size(yM,1)+size(xM,2)*size(yB,1)+size(xR,2)*size(yM2,1);
bpp=zeros(bpparticles,dim);
j=dist;
i=1;
l=2;
for n=1:size(xL,2)
    for k=1:size(yM,1)
        bpp(i,:)=[xL(1,l) yM(k,1)-.5*j];
        i=i+1;
    end
    if j==dist;
        j=0;
    else
        j=dist;
    end
    if l==1
        l=2;
    else
        l=1;
    end
end
j=0;
l=1;
for k=1:size(yB,1)
    for n=1:size(xM,2)
        bpp(i,:)=[xM(1,n)-.5*j yB(l,1)];
        i=i+1;
    end
    if j==dist;
        j=0;
    else
        j=dist;
    end
    if l==1
        l=2;
    else
        l=1;
    end
end
j=0;
l=2;
for n=1:size(xR,2)
    for k=1:size(yM2,1)
        bpp(i,:)=[xR(1,l) yM2(k,1)-.5*j];
        i=i+1;
    end
    if j==dist;
        j=0;
    else
        j=dist;
    end
    if l==1
        l=2;
    else
        l=1;
    end
end

boundaryparticles=size(bpp,1);
totalpp=cat(1,pp,bpp);
% % boundaryparticles=0;
% % totalpp=pp;
%%%%% density parameters
rho0=1000;

mtotal=rho0*height*width;        % total mass of dam
mi=mtotal/particles;             % mass of each particle
dv=mi/rho0; % volume of each particle
rho= ones(particles,1).*rho0;
m=dv*rho; % vector of mass

%%%%%%%%%%%%%%%%%%%%%%%%%%%% boundary particle density init.
% defined with the same mass as the fluid particles and the same density
mass=ones(particles+boundaryparticles,1).*m(1);
rho= ones(particles+boundaryparticles,1).*rho0; % previous iteration values
rhorhs=ones(particles+boundaryparticles,1).*rho0;% current iteration values

%%%%% pressure parameters
pressure = zeros(particles+boundaryparticles,1);  % current value of pressure
c0 =10.0*sqrt(2*9.81*height);
gamma=7;
B =rho0*c0*c0/gamma;

%%%%%%%%%%%%%%%%%%%%%%%%%% velocity parameters
velocity=zeros(particles+boundaryparticles,dim);

%%%%%%%%%%%%%%%%%%%%%%%%%% viscosity parameters
alpha= .1;
beta=0;  %%% if Mach number is less then about 5   % ref chapter 3 pg 21
epsilon=.01;  %% just so you dont divide by zero

%%%%%%%%%%%%%%  force parameters
totalforce=zeros(particles+boundaryparticles,dim);
gravity = -9.81;






%%%%%%%%%%%%%%%%   time-step init.
deltatime=0;
dt = h/c0;
dtstart=dt;
dtgraph=.01;

time2=0;
iterations=1;
%%%%%%%%%%%    plot configuration
fig=figure('Name','Scatter');
colormap(fig,'winter');
set(fig,'Visible','off')
ax = axes('Parent',fig);
ax.XLim = [-.25 (containerwidth+.25)];ax.YLim = [-.25 (containerheight+.25)];
ax.XLimMode = 'manual';ax.YLimMode = 'manual';
hg=hggroup('Parent',ax);
bc= [containerwidth+h/2 containerheight+h/2];
bpplot=scatter(ax,totalpp(particles+1:particles+boundaryparticles,1),totalpp(particles+1:particles+boundaryparticles,2), .04*300*2*dist, [0,0,0], 'filled','MarkerEdgeColor',[0,0,0],'Parent',hg);
spplot=scatter(ax,totalpp(1:particles,1),totalpp(1:particles,2), .04*300*2*dist, pressure(1:particles), 'filled','MarkerEdgeColor','none','Parent',hg);%yellow is the highest pressure
lplot=line(totalpp(round(particles/4),1),totalpp(round(particles/4),2),'color',[.5 .5 .5],'marker','o','linestyle','none','markersize',75*2*h,'Parent',hg);
ax.Title=title(sprintf('Time = %g dt = %g',time2,dt),'Parent',ax);
ax.Parent=fig;
OutputName = sprintf('%d.png',0);
saveas(fig, OutputName);


display('running.....')
tic;
lineparticle=round(particles/4);
l=0;
totaliterations=0;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
while(iterations<=225)
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  force calculations
    velocity1=velocity;
    rho1=rho;
    [rho,velocity]=RHSEVAL(totalpp, mass ,pressure, h, alpha, beta, c0,epsilon,particles,rho0,gravity,rhorhs,dt,velocity1,rho1,rho,velocity,totalforce);
    pressure=B.*((rho./rho0).^gamma-1);
    totalpp=totalpp+velocity.*dt;
    
    time2=time2+dt;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   Time-step condition
    maxv=max(velocity(:));
    dt=max(min(h/(maxv*50), dtstart), 1e-5);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   plot update
    
    if time2>=dtgraph
        pps=totalpp(1:particles,1);
        maxx=max(pps);
        %%% refreshing graph
        set(spplot,'XData',totalpp(1:particles,1),'YData',totalpp(1:particles,2),'CData',real(log(pressure(1:particles)+1)))%color: 'CData',pressure(1:particles)
        set(lplot,'XData',totalpp(lineparticle,1),'YData',totalpp(lineparticle,2))
        set(ax,'Title',title(sprintf('Time = %g dt = %g max position= %g',time2,dt,maxx)))
        refreshdata(fig)
        %%%%% generating output file
        OutputName = sprintf('%d.png',iterations);
        saveas(fig, OutputName);
        %%%% prepare for next output file
        dtgraph=dtgraph+.01;
        iterations=iterations+1;
    end
%     l=l+1;
%     if l>=20
%     rho(1:particles) = rho0; 
%     l=0;
%     end
totaliterations=totaliterations+1;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
t=datetime('now');
DateString = datestr(t);
directoryname = strcat(DateString,sprintf('_h=%g_particles=%g_particledensity=%g_dist=%g_iterations=%g',h,particles,particledensity,dist,totaliterations));
[s,mess,messid] = mkdir('Videos/',directoryname);
path=strcat('Videos/',directoryname);
% s =
%    1
% mess =
%    Directory "newdir" already exists.
% messid =
%    MATLAB:MKDIR:DirectoryExists

%%%%%%%%% generating a video file %%%%%%%%%%%%%%%%%%%%
time=sprintf('_time=%g',toc);
dirname2=strcat(directoryname,time);
writerObj = VideoWriter(dirname2);
open(writerObj);
for K = 0:iterations-1
    filename = sprintf('%d.png', K);
    thisimage = imread(filename);
    writeVideo(writerObj, thisimage);
    movefile(filename, path);
end
close(writerObj);
movefile(strcat(dirname2,'.avi'), path);
display('finished')

clear all,clc
